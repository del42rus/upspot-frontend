import moment from 'moment';
import Vue from 'vue';

Vue.use({
  install: Vue => {
    Vue.prototype.$moment = (...args) => {
      if (args.length) {
        return moment(...args)
      }

      return moment
    }
  }
});
