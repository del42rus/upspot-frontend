import Vue from 'vue';

import Navigation from '~/components/Navigation.vue'
import ProposalComments from "../components/ProposalComments";
import Comment from "../components/Comment"
import ProposalParticipants from "../components/ProposalParticipants";
import ProposalTimetable from "../components/ProposalTimetable";
import Avatar from "../components/Avatar"

Vue.component('navigation', Navigation);
Vue.component('proposal-comments', ProposalComments);
Vue.component('proposal-participants', ProposalParticipants);
Vue.component('proposal-timetable', ProposalTimetable);
Vue.component('comment', Comment);
Vue.component('avatar', Avatar);
