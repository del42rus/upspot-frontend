import * as types from '../mutation-types';

export const state = () => ({
  proposals: [],
  proposal: null,
  pagination: {
    perPage: 20
  },
  comments: [],
  comment: null,
  commentParentThread: [],
  participants: [],
  timetable: [],
  showDrawer: false
});

export const getters = {
  isLoggedIn(state) {
    return state.auth.loggedIn;
  },
  loggedInUser(state) {
    return state.auth.user;
  }
};

export const mutations = {
  [types.SET_PROPOSALS](state, payload) {
    state.proposals = payload.proposals;
  },
  [types.SET_PROPOSAL](state, payload) {
    state.proposal = payload.proposal;
  },
  [types.SET_PAGINATION](state, payload) {
    state.pagination = { ...state.pagination, ...payload.pagination };
  },
  [types.SET_COMMENTS](state, payload) {
    state.comments = payload.comments;
  },
  [types.ADD_COMMENT](state, payload) {
    payload.comment.is_liked = false;
    state.comments = [ payload.comment, ...state.comments]
  },
  [types.INCREASE_PROPOSAL_COMMENTS_COUNT](state) {
    if (state.proposal) {
      state.proposal.comments_count++;
    }
  },
  [types.INCREASE_COMMENT_REPLIES_COUNT](state) {
    if (state.comment) {
      state.comment.replies_count++;
    }
  },
  [types.SET_COMMENT](state, payload) {
    state.comment = payload.comment;
  },
  [types.SET_COMMENT_PARENT_THREAD](state, payload) {
    state.commentParentThread = payload.thread;
  },
  [types.SET_PROPOSAL_PARTICIPANTS](state, payload) {
    state.participants = payload.participants;
  },
  [types.SET_PROPOSAL_TIMETABLE](state, payload) {
    state.timetable = payload.timetable;
  },
  [types.LIKE_PROPOSAL](state, payload) {
    payload.proposal.is_liked = true;
  },
  [types.UNLIKE_PROPOSAL](state, payload) {
    payload.proposal.is_liked = false;
  },
  [types.LIKE_COMMENT](state, payload) {
    payload.comment.is_liked = true;
    payload.comment.likes_count++;
  },
  [types.UNLIKE_COMMENT](state, payload) {
    payload.comment.is_liked = false;
    payload.comment.likes_count = payload.comment.likes_count >= 0 ? payload.comment.likes_count - 1: 0;
  },
  [types.SET_TIME_PARTICIPANTS](state, payload) {
    payload.time.participants = payload.participants;
  },
  [types.UPDATE_PROFILE](state, payload) {
    state.auth.user.name = payload.user.name;
    state.auth.user.username = payload.user.username;
    state.auth.user.email = payload.user.email;
    state.auth.user.gender = payload.user.gender;
  },
  [types.SHOW_DRAWER](state, payload) {
    state.showDrawer = payload.value;
  },
};

export const actions = {
  async fetchProposals({ state, commit }, params) {
    let defaultParams = {
      page: 1,
      per_page: state.pagination.perPage
    };

    const data = await this.$axios.$get('/proposals', {
      params: { ...defaultParams, ...params }
    });

    commit(types.SET_PROPOSALS, {
      proposals: data._embedded.proposals
    });

    commit(types.SET_PAGINATION, {
      pagination: {
        page: data._page,
        pageCount: data._page_count,
        total: data._total_items,
      }
    })
  },
  async fetchProposal({ state, commit }, proposalId) {
    const proposal = await this.$axios.$get(`/proposals/${proposalId}`);

    commit(types.SET_PROPOSAL, { proposal });
  },
  async fetchComments({ state, commit }, comment) {
    commit(types.SET_COMMENTS, { comments: [] });

    let data;

    if (comment) {
      commit(types.SET_COMMENT, { comment });
      commit(types.SET_COMMENT_PARENT_THREAD, { thread: (await this.$axios.$get(`/comments/${comment.id}/parent-thread`))._embedded.parent_thread })
      data = await this.$axios.$get(`/comments/${comment.id}/replies`);
    } else {
      commit(types.SET_COMMENT, { comment: null });
      commit(types.SET_COMMENT_PARENT_THREAD, { thread: [] });

      data = await this.$axios.$get('/comments', {
        params: {
          proposal_id: state.proposal.id
        }
      });
    }

    const $axios = this.$axios;

    const comments = data._embedded.comments.map(async function(comment) {
      comment.replies = (await $axios.$get(`/comments/${comment.id}/replies?per_page=1`))._embedded.comments;

      return comment;
    });

    for (const comment of comments) {
      commit(types.SET_COMMENTS, { comments: [...state.comments, await comment] })
    }
  },
  async fetchParticipants({ state, commit }) {
    const data = await this.$axios.$get('/participants', {
      params: {
        proposal_id: state.proposal.id
      }
    });

    commit(types.SET_PROPOSAL_PARTICIPANTS, { participants: data._embedded.participants })
  },
  async fetchTimetable({ state, commit }) {
    const timetable = (await this.$axios.$get('/timetable', { params: { proposal_id: state.proposal.id } }))
      ._embedded.timetable.map((time) => {
        time.participants = [];

        return time;
      });

    (await this.$axios.$get('/participants', { params: { time_id: timetable.reduce((ids, time) => { return [...ids, time.id] }, []) } }))
      ._embedded.participants.forEach((participant) => {
        timetable.map((time) => {
          if (time.id === participant.time_id) {
            time.participants.push(participant);
          }
          return time;
        });
      });

    commit(types.SET_PROPOSAL_TIMETABLE, { timetable })
  },
  async addComment({ state, commit }, comment) {
    const newComment = await this.$axios.$post('/comments', comment);

    commit(types.ADD_COMMENT, { comment: newComment })
  },
  async likeProposal({ commit }, proposal) {
    try {
      await this.$axios.$get(`/proposals/${proposal.id}/like`);
      commit(types.LIKE_PROPOSAL, { proposal });

    } catch (e) {

    }
  },
  async unlikeProposal({ commit }, proposal) {
    await this.$axios.$get(`/proposals/${proposal.id}/unlike`);
    commit(types.UNLIKE_PROPOSAL, { proposal })
  },
  async likeComment({ commit }, comment) {
    try {
      await this.$axios.$get(`/comments/${comment.id}/like`);
      commit(types.LIKE_COMMENT, { comment })
    } catch (e) {

    }
  },
  async unlikeComment({ commit }, comment) {
    try {
      await this.$axios.$get(`/comments/${comment.id}/unlike`);
      commit(types.UNLIKE_COMMENT, { comment })
    } catch (e) {

    }
  },
  async participate({ state, commit }, time) {
    await this.$axios.$get(`/timetable/${time.id}/participate`);
    commit(types.SET_TIME_PARTICIPANTS, {
      time,
      participants: (await this.$axios.$get(`/timetable/${time.id}/participants`))._embedded.participants
    });

    const data = await this.$axios.$get('/participants', {
      params: {
        proposal_id: state.proposal.id
      }
    });

    commit(types.SET_PROPOSAL_PARTICIPANTS, {
      participants: data._embedded.participants
    })
  },
  async withdraw({ state, commit }, time) {
    await this.$axios.$get(`/timetable/${time.id}/withdraw`);

    commit(types.SET_TIME_PARTICIPANTS, {
      time,
      participants: (await this.$axios.$get(`/timetable/${time.id}/participants`))._embedded.participants
    });

    const data = await this.$axios.$get('/participants', {
      params: {
        proposal_id: state.proposal.id
      }
    });

    commit(types.SET_PROPOSAL_PARTICIPANTS, {
      participants: data._embedded.participants
    })
  },
};

