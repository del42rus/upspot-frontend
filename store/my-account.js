import * as types from '../mutation-types';

export const state = () => {
  return {
    proposal: null,
  }
};

export const getters = {

};

export const mutations = {

};

export const actions = {
  async updateProfile({ state, commit }, data) {
    await this.$axios.$put('/users/self', data );

    commit(types.UPDATE_PROFILE, { user: data }, { root: true })
  },
};
